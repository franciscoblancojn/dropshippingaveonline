<?php
/*
Plugin Name: Drop Shipping Aveonline
Plugin URI: https://gitlab.com/franciscoblancojn/dropshippingaveonline
Description: Integración de woocommerce con los servicios de prodcutos de Aveonline.
Author: Francisco Blanco
Version: 1.0.11
Author URI: https://franciscoblanco.vercel.app/
*/
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/dropshippingaveonline',
	__FILE__,
	'dropshippingaveonline'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

define("DSAV_LOG",true);
define("DSAV_PATH",plugin_dir_path(__FILE__));
define("DSAV_URL",plugin_dir_url(__FILE__));

require_once DSAV_PATH . "src/index.php";