<?php
function DSAV_create_menu() {

	//create new top-level menu
	add_menu_page('DSAV Settings', 'DSAV', 'administrator', __FILE__, 'DSAV_settings_page' , DSAV_URL.'src/img/DSAV.png' );

	//call register settings function
	add_action( 'admin_init', 'register_DSAV_settings' );
}
add_action('admin_menu', 'DSAV_create_menu');


function register_DSAV_settings() {
	//register our settings
	register_setting( 'DSAV-settings-group', 'new_option_name' );
	register_setting( 'DSAV-settings-group', 'some_other_option' );
	register_setting( 'DSAV-settings-group', 'option_etc' );
}

function DSAV_settings_page() {
    if($_POST["DSAV_Save"]=="Save"){
        update_option("DSAV_Token",$_POST["DSAV_Token"]);

        $api = new DSAV_apiAveonline();

        $api->log(array(
            "token"=>$_POST["DSAV_Token"],
            "urls"=>array(
                "createProduct" => DSAV_URL."src/routes/create-product.php",
                "updateProduct" => DSAV_URL."src/routes/update-product.php",
                "deleteProduct" => DSAV_URL."src/routes/delete-product.php",
            ),
        ));
    }
    ?>
    <div class="wrap">
        <h1>
            Drop Shipping Aveonline
        </h1>
        <form method="post">
            <label>
                Token
                <br/>
                <input type="text" value="<?=get_option("DSAV_Token")?>" name="DSAV_Token" id="DSAV_Token" />
            </label>
            <br>
            <br>
            <input type="submit" name="DSAV_Save" value="Save" class="button action"/>
        </form>
    </div>
    <?php 
}