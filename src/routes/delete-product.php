<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
header('Content-Type: application/json; charset=utf-8');
if(isset($data)){
    $_POST = $data;
}

$product_id = $_POST["product_id"];
$token = $_POST["token"];
try {
    if( get_option("DSAV_Token") != $token ){
        throw new Exception('Token invalid');
    }
    if( empty( $product_id) ) {
        throw new Exception('Product id Required');
    }
    $result = DSAV_deleteProduct($product_id);
    echo json_encode(array(
        "status" => 200,
        "data" => $result
    ));
} catch (Exception $e) {
    echo json_encode(array(
        "status" => 400,
        "data" => $e->getMessage()
    ));
}