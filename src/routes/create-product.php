<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
header('Content-Type: application/json; charset=utf-8');
if(isset($data)){
    $_POST = $data;
}

$products = $_POST["products"];
$token = $_POST["token"];
try {
    if( get_option("DSAV_Token") != $token ){
        throw new Exception('Token invalid');
    }
    if( empty( $products) ) {
        throw new Exception('Products Required');
    }
    if( !is_array($products) ) {
        throw new Exception('Products is not array');
    }
    if( count($products) == 0 ) {
        throw new Exception('Products is not array');
    }
    $result = DSAV_createProducts($products);
    echo json_encode(array(
        "status" => 200,
        "data" => $result
    ));
} catch (Exception $e) {
    echo json_encode(array(
        "status" => 400,
        "data" => $e->getMessage()
    ));
}