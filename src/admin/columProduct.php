<?php

add_filter( 'manage_edit-product_columns', 'DSAV_woo_product_aveonline_id_column', 20 );
function DSAV_woo_product_aveonline_id_column( $columns ) {
    $columns['aveonline_id'] = esc_html__( 'Aveonline ID', 'woocommerce' );
    return $columns;
}

add_action( 'manage_product_posts_custom_column', 'DSAV_woo_product_aveonline_id_column_data', 2 );
function DSAV_woo_product_aveonline_id_column_data( $column ) {
    global $post;
    if ( $column == 'aveonline_id' ) {
        echo get_post_meta($post->ID,'id_aveonline_drop_shipping',true);      
    }
}