<?php

function DSAV_msjProduct(){
    global $pagenow;
    if ( $pagenow == 'post.php' ) {
        $porduct_id = $_GET["post"];
        $id_aveonline_drop_shipping = get_post_meta($porduct_id,"id_aveonline_drop_shipping",true);

        if($id_aveonline_drop_shipping === false || $id_aveonline_drop_shipping == null || $id_aveonline_drop_shipping == ""){
            return;
        }
        echo '
            <div class="notice notice-warning is-dismissible">
                <p style="font-size:26px;">Este porducto es un producto Aveonline, Recomendamos no Modificarlo</p>
            </div>
        ';
    }
}
add_action('admin_notices', 'DSAV_msjProduct');