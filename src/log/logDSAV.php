<?php
if(DSAV_LOG){
    function add_DSAV_LOG_option_page($admin_bar)
    {
        global $pagenow;
        $admin_bar->add_menu(
            array(
                'id' => 'DSAV_LOG',
                'title' => 'DSAV_LOG',
                'href' => get_site_url().'/wp-admin/options-general.php?page=DSAV_LOG'
            )
        );
    }

    function DSAV_LOG_option_page()
    {
        add_options_page(
            'Log Drop Shipping Aveonline',
            'Log Drop Shipping Aveonline',
            'manage_options',
            'DSAV_LOG',
            'DSAV_LOG_page'
        );
    }

    function DSAV_LOG_page()
    {
        $log = get_option("DSAV_LOG");
        if($log === false || $log == null || $log == ""){
            $log = "[]";
        }
        ?>
        <script>
            const log = <?=$log?>;
        </script>
        <h1>
            Solo se guardan las 100 peticiones
        </h1>
        <pre><?php var_dump(array_reverse(json_decode($log,true)));?></pre>
        <?php
    }
    add_action('admin_bar_menu', 'add_DSAV_LOG_option_page', 100);

    add_action('admin_menu', 'DSAV_LOG_option_page');

}

function addDSAV_LOG($newLog)
{
    if(!DSAV_LOG){
        return;
    }
    $log = get_option("DSAV_LOG");
    if($log === false || $log == null || $log == ""){
        $log = "[]";
    }
    $log = json_decode($log);
    $log[] = $newLog;
    $log = array_slice($log, -100,100); 
    update_option("DSAV_LOG",json_encode($log));
}