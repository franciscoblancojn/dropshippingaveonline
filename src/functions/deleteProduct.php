<?php

/**
 * DSAV_deleteProduct
 * @param product_id
 */

function DSAV_deleteProduct($product_id)
{
    try {
        $args = array(
            'post_type' => 'product',
            'meta_key' => 'id_aveonline_drop_shipping',
            'meta_value' => $product_id
        );
        $products = wc_get_products($args);
        if(count($products) == 0){
            return array(
                "status" => 400,
                "data" => "Product id [".$product_id."] Not Exist"
            );
        }
        $product = $products[0];
        wp_delete_post( $product->get_id() );

        return array(
            "status" => 200,
            "product_delete" => $product_id
        );
    } catch (Exception $e) {
        return (array(
            "status" => 400,
            "data" => $e->getMessage(),
            "product_delete" => $product_id
        ));
    }

}