<?php

require_once DSAV_PATH . "src/functions/createProduct.php";
require_once DSAV_PATH . "src/functions/validateField.php";
require_once DSAV_PATH . "src/functions/uploadImg.php";
require_once DSAV_PATH . "src/functions/createCategories.php";
require_once DSAV_PATH . "src/functions/createTags.php";
require_once DSAV_PATH . "src/functions/addTerm.php";
require_once DSAV_PATH . "src/functions/deleteProduct.php";
require_once DSAV_PATH . "src/functions/updateProduct.php";