<?php


class DSAV_apiAveonline
{
    private $URL_API = "https://logs-yp4l.onrender.com/";
    

    public function __construct($settings = array())
    {
        $this->settings = $settings;
    }

    private function request($json , $url="")
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->URL_API.$url,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_VERBOSE => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($json),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        $response = json_decode($response,true);

        addDSAV_LOG(array(
            "url"=>$URL_API.$url,
            "send"=>$json,
            "result"=>$response
        ));
        return $response;
    }

    public function log($data)
    {
        return $this->request($data);
    }
}
